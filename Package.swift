// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 8.2.5
let package = Package(
    name: "FreestarAds-Fyber2",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Fyber2",
            targets: [
                "FreestarAds-Fyber2",
                "FreestarAds-Fyber2-Core",
                "IASDKCore"
                ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.17.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Fyber2",
            url: "https://gitlab.com/freestar/spm-freestarads-fyber/-/raw/8.2.5/FreestarAds-Fyber2.xcframework.zip",
            checksum: "510a752a9947a3f54584127374ffa93921a432b9f8c260a91e295e00cefaf14f"
            ),
        .target(
            name: "FreestarAds-Fyber2-Core",
            dependencies: [
                .product(name: "FreestarAds", package: "spm-freestarads-core")
            ],
            resources: [
                .process("Resources")
            ]
            ),
        .binaryTarget(
            name: "IASDKCore",
            url: "https://gitlab.com/freestar/spm-freestarads-fyber/-/raw/8.2.5/IASDKCore.xcframework.zip",
            checksum: "0e3b6d747d2c6b960d0bed7dd2469b1ef3a90d93e8c206e826e3cd288539e578"
        )
    ]
)
